$(document).ready(function(){
  $('#loading').show();
  $('#empty-gift').hide();
  $('#gift-list').hide();
  $('#gift-detail').hide();

  //https://qs.test.dtac.co.th/giftlists/lists/th
  $.post("../getGiftListsAPI", 'lang='+lang, function(response, status){
    //console.log(response);
    if(response.status.resCode == '0'){

      var giftRedeemableListHTML =  '';
      var giftRedeemableList = response.data.giftRedeemableList;
      //console.log('value');
      $.each( giftRedeemableList, function( key, value ) {
        //console.log(value);
        var gift_logo = value.cmpgDetails.logo;
        var gift_title = value.prizeTitle;

        var btn_text = redeem;
        var redmStatus = redeem_end;       
        var gift_date = value.cmpgDetails.cmpgRedmEndDTTM;
        gift_date = gift_date.split('T');
        gift_date = gift_date[0];
        var markFlag_class = 'redeemabled';
        if(value.markFlag == 'Y'){ 
          var btn_text = redeemed;
          var redmStatus = redeemed;       
          var gift_date = value.markUsedDate;
          var markFlag_class = 'redeemed';
        }
        var expire_class = '';
        if(value.expireStatus == 'Y'){
          var expire_class = ' style="color:#de000;"';
        }

        giftRedeemableListHTML = '<div class="row"><div class="col-2"><img src="'+gift_logo+'" class="img-fluid"></div><div class="col-7 giftlist-padding">'+gift_title+'<br><span'+expire_class+'>'+redmStatus+' '+gift_date+'</span></div><div class="col-3 giftlist-padding  text-center"><div class="'+markFlag_class+'" id="'+markFlag_class+'-'+key+'">'+btn_text+'</div></div></div>';
        
        $('#gift-list-redeemable').append(giftRedeemableListHTML);
        
        $('#redeemabled-'+key).click(function(){

          console.log($(this).attr('id'));
          console.log(value);

          //gift-detail-image
          
          $('#gift-detail-image').attr('src',gift_logo);
          $('#gift-detail-title').text(gift_title);
          $('#gift-detail-subtitle').text(redmStatus+' '+gift_date);
          $('#gift-detail-redeem-btn').html('<a href="'+value.redmURL+'">'+redeem+'</a>');
          $('#gift-detail-description').html(value.prizeDesc);
          $('#gift-detail-terms').html(value.cmpgDetails.termsConds);

          $('#gift-detail').show();
          $('#gift-list').hide();
        });
      });


      var giftUnredeemableListHTML =  '';
      var giftUnredeemableList =  response.data.giftUnredeemableList;
      $.each( giftUnredeemableList, function( key, value ) {
        //console.log(value);
        var gift_logo = value.cmpgDetails.logo;
        var gift_title = value.prizeTitle;

              
        var gift_redeemed_date = value.cmpgDetails.cmpgRedmEndDTTM;
        gift_redeemed_date = gift_redeemed_date.split('T');
        gift_redeemed_date = gift_redeemed_date[0];

        
        var btn_text = expired;
        var redmStatus = expired_redeemed_on;

        giftUnredeemableListHTML = '<div class="row"><div class="col-2"><img src="'+gift_logo+'" class="img-fluid"></div><div class="col-7 giftlist-padding">'+gift_title+'<br><span>'+redmStatus+' '+gift_redeemed_date+'</span></div><div class="col-3 giftlist-padding text-center"><div class="reedeemed">'+btn_text+'</div></div></div>';

        $('#gift-list-redeemed').append(giftUnredeemableListHTML);
      });

      $('#loading').hide();
      if(giftUnredeemableListHTML =='' && giftRedeemableListHTML == ''){
        $('#empty-gift').show();
      }else{
        $('#gift-list').show();

      }
    }
  }, 'json');

});
