﻿<?php 
/*


<color name="red">#DE0000</color>
<color name="midBlue">#1aaae2</color>
*/
// LANGUAGE
	$title = 'กล่องของขวัญ';
	$txt_empty_gift = 'ขออภัยค่ะ, คุณยังไม่ได้รับของขวัญในตอนนี้';
	$txt_redeemable = 'ของขวัญที่สามารถใช้ได้';
	$txt_redeemed = 'ของขวัญที่ใช้แล้ว / หมดอายุ';
	$txt_loading = 'กรุณารอสักครู่';
	$txt_backlink = 'ย้อนกลับ';

	$txt_js['redeem'] = 'รับสิทธิ์';
	$txt_js['redeemed'] = 'ใช้แล้ว';
	$txt_js['expired'] = 'หมดอายุ';
	$txt_js['campaign_end'] = 'กิจกรรมหมดอายุ ';
	$txt_js['redeem_end'] = 'รับสิทธิ์ได้ถึง ';
	$txt_js['expired_redeemed_on'] = 'รับสิทธิ์/หมดอายุไปแล้วเมื่อ ';
	$txt_js['campaign_days_left'] = 'เหลือเวลาอีก ';

if($lang == 'EN'){
	$title = 'Gift';
	$txt_empty_gift = 'Sorry, you do not have any gift at the moment.';
	$txt_redeemable = 'Redeemable';
	$txt_redeemed = 'Redeemed / Expired Gift ';
	$txt_loading = 'Loading ';
	$txt_backlink = 'Back';

	$txt_js['redeem'] = 'Redeem';
	$txt_js['redeemed'] = 'Redeemed';
	$txt_js['expired'] = 'Expired';
	$txt_js['campaign_end'] = 'Campaign expires on ';
	$txt_js['redeem_end'] = 'Valid to redeem until ';
	$txt_js['expired_redeemed_on'] = 'Redeemed/Expired on ';
	$txt_js['campaign_days_left'] = ' day(s) left';
}
?><!doctype html>
<html>
<head>
<title><?php echo $title; ?></title>
<meta charset="UTF-8">
<!-- Use the latest (edge) version of IE rendering engine -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" />
<!-- Forcing initial-scale shouldn't be necessary -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<link href="<?php echo $root; ?>assets/css/bootstrap4.min.css" rel="stylesheet" />
<link href="<?php echo $root; ?>assets/css/animate.min.css" rel="stylesheet"/>
<link href="<?php echo $root; ?>assets/css/style.css" rel="stylesheet" />
</head>
<body>

<section class="container-fluid" id="empty-gift">
<div class="container">

	<div class="row">		
		<div class="col-12 txt-title">
			<h6><?php echo $txt_empty_gift; ?></h6>
		</div>
	</div>

</div>
</section>

<section class="" id="gift-list">
<div class="container">

	<div class="row">		
		<div class="col-12 txt-title">
			<h6><?php echo $txt_redeemable; ?></h6>
		</div>
	</div>
	<div class="-gift-list-redeemable" id="gift-list-redeemable">
	</div>
		
	<div class="row">		
		<div class="col-12 txt-title">
			<h6><?php echo $txt_redeemed; ?></h6>
		</div>
	</div>
	<div class="-gift-list-redeemed" id="gift-list-redeemed">
	</div>

</div>
</section>

<section class="container-fluid" id="gift-detail">
<div class="container">

	<div class="row -gift-detail-title">
		<div class="col-3">
			<img src="" class="img-fluid" id="gift-detail-image">
		</div>
		<div class="col-9">
			<h4 id="gift-detail-title"></h4>
			<h6 id="gift-detail-subtitle" class="redeemabled"></h6>
		</div>
	</div>

	<div class="row -gift-detail-redeem-btn">
		<div class="col-6 offset-3" id="gift-detail-redeem-btn">
		</div>
	</div>

	<div class="row -gift-detail-description">
		<div class="col-12" id="gift-detail-description">
		</div>
	</div>

	<div class="row -gift-detail-terms">
		<div class="col-12" id="gift-detail-terms">
		</div>
	</div>

</div>
</section>


<section class="" id="loading">
<div class="container">
	<div class="row">
		<div class="col-12 text-center">
			<img src="<?php echo $root; ?>assets/images/propeller.png">
		</div>
		<div class="col-12 text-center">
			<?php echo $txt_loading; ?> . . .
		</div>
	</div>
</div>
</section>

<?php if($backlink != ''){ ?>
<section class="section-space" id="back-to-spin">
<div class="container-fluid">
	<div class="row">
		<div class="col-12 text-center">
			<a href="<?php echo $backlink; ?>" class="the-blues"><?php echo $txt_backlink; ?></a>
		</div>
	</div>	
</div>
</section>
<?php } ?>

<!--   Core JS Files   -->
<script>
var lang = '<?php echo $lang; ?>';
<?php foreach($txt_js AS $key=>$value){ 
	echo "var ",$key, " = '", $value,"';";
} ?>
</script>
<script src="<?php echo $root; ?>assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="<?php echo $root; ?>assets/js/bootstrap4.min.js" type="text/javascript"></script>
<script src="<?php echo $root; ?>assets/js/script.js" type="text/javascript"></script>

</body>
</html>