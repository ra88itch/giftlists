<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library(array('encrypt'));
		$this->load->helper(array('cookie', 'url'));
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index(){
		if(isset($_GET['backlink']) && $_GET['backlink'] != ''){
			$backlink = $_GET['backlink'];
			$expires = 2628000;
			set_cookie('tac_gift_list', $backlink, $expires);
		}
		
		redirect('lists/th');		
	}

	function th(){
		$response['root'] =  $this->config->item('base_url');
		$response['lang'] =  'TH';

		$response['backlink'] = '';
		$backlink = get_cookie('tac_gift_list', true);
		if ($backlink != null) {
			$response['backlink'] =  $backlink;
		}
		$this->load->view('lists', $response);
	}

	function en(){
		$response['root'] =  $this->config->item('base_url');
		$response['lang'] =  'EN';

		$response['backlink'] = '';
		$backlink = get_cookie('tac_gift_list', true);
		if ($backlink != null) {
			$response['backlink'] =  $backlink;
		}
		$this->load->view('lists', $response);
	}
}
