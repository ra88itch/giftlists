<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GetGiftListsAPI extends CI_Controller {

    function __construct() {
        parent::__construct();

		$this->load->helper(array('cookie', 'url'));
	}
	
	public function index(){
        $campaignToken = get_cookie('campaign_token', true);
        if ($campaignToken != null) {		
			$campaignToken = json_decode($campaignToken);

			$expiredDttm = DATE('Y-m-d H:i:s', strtotime($campaignToken->expiredDttm));
			$now = DATE('Y-m-d H:i:s');

			if($expiredDttm < $now){
				// Redirect to get new stamp
				redirect('campaignTokenExpired');
			}
			
			$lang = 'EN';
			if(isset($_POST['lang']) && $_POST['lang'] == 'TH'){
				$lang = 'TH';			
			}
            $this->getGiftLists($lang, $campaignToken);
		}
	}
	
	private function getGiftLists($lang, $getCampaignToken){
		$stamp = $getCampaignToken->token;
		//$stamp = 'eyJhbGciOiJIUzI1NiJ9.eyJpYXQiOjE1NTMwNzQyNjQsInVzciI6IkFQSUdXIiwiZ3VwIjoiNSIsImFwaWlkIjoiMjAwMHwzMDAwfDExMTAwIiwic3RhbXAiOiIzNTVlYTMwOTE1NGVlYmE2MGZhNGViMDc0MDc0MWMwYTJhZjZhNDk0ZmY5YTA0MTg5ZmI1YzI2ZGE5YmI4YTg0YjdiYWUzODBjMjY1ZDkyZjYyNTVhNTc1NTM0ZTNhNjVjNjc3MTliNjA2MTM4MDFhMGRhMjNjOGJiZGZmZGU0ZTIxN2NiZmY5MDM3YzRjYmVkMTQzY2I2YmY1NmMwZWQ4MzBmMjY1Njg1MmE0ZGNlODk3NmJkMzEyYjIxNTNlYWUiLCJ1c2Vya2V5IjoiNjY5NTEwNjkxOTkuNjI1MTEwNDMwIiwidXNlcmRldmljZWlkIjoiLSIsImV4cCI6MjE4Mzc5NDI2NH0.KdUGmOD-xmugp62SwFu7MxV1biNAob_MEzI7e_QaLp8';
		$url = 'http://digitaltest.dtac.co.th/mobileapi/campaign/gift/v2.1.0/gift?lang='.$lang;

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, Array("authorization:Bearer ".$stamp));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$giftListsJSON = curl_exec($ch);
		curl_close($ch);
		
		echo $giftListsJSON;
	}
}
